'use strcit';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getView(resolve, reject) {
    var options = {
        name: 'view',
        template: 'templates/view.html',
        data: function () {
            return {
                enableCtrl: false
            };
        },
        mixins: [
            common.updateCanvasSize,
            app.pen
        ],
        created: function () {
            var context = this;
            context.$on('move', function (left, top) {
                // left > 0 右
                // left < 0 左
                // top > 0 下
                // top < 0 上
                console.info('move', left, top);
            });
        },
        mounted: function () {
            detectGesture(this);
        },
        methods: {
            zoomIn: function (event) {
                console.log('放大', event);
            },
            zoomOut: function (event) {
                console.log('缩小', event);
            },
            zoom: common.throttle(function (event) {
                var context = this;
                var deltaY = event.deltaY;
                var action = deltaY < 0 ? 'zoomIn' : 'zoomOut';
                if(Math.abs(deltaY) > 0) {
                    context[action](event);
                }
            }, 80),
            popup: app.popup
        }
    };
    asyncComponent(options, resolve, reject);
}

function detectGesture(context) {
    context.$nextTick(function () {
        var canvas = context.$refs['pen-canvas'];
        if(!canvas) {
            return;
        }
        var region = new ZingTouch.Region(canvas);
        region.bind(canvas, 'pinch', function (event) {
            context.zoomOut(event);
        }); 
        region.bind(canvas, 'expand', function (event) {
            context.zoomIn(event);
        }); 
    });
}

app.components.getView = getView;

})(app);
