'use strict';

var app = {
    components: {},
    _debug: {}
};

(function (app) {

function getEnv() {
    var env = {
        '127.0.0.1': 'development',
        'localhost': 'development',
        'static.f2e.yiifcms.com': 'development'
    };
    return env[location.hostname] || 'production';
}

var urlMap = {};

urlMap.development = {
    multilevel: 'mock/multilevel.json'
};

// 生产环境的接口地址
urlMap.production = {

};

function mountDialog(options, callback) {
    $.get(options.template, function (html) {
        $.extend(options, {template: html});
        var Dialog = Vue.extend(options);
        var instance = new Dialog({
            el: document.createElement('div')
        });
        $('body').append(instance.$el);
        $.isFunction(callback) && callback(instance);
    });
}

function asyncComponent(options, resolve, reject) {
    var url = options.template;
    asyncComponent.cache = asyncComponent.cache || {};
    
    var promise;
    if(asyncComponent.cache[url]) {
        promise = $.Deferred().resolve(asyncComponent.cache[url]).promise();
    } else {
        promise = $.get(url);
    }

    promise.then(function (html) {
        asyncComponent.cache[url] = html;
        var component = $.extend({}, options, {template: html});
        resolve(component);
    }).fail(function (xhr, status, error) {
        reject(error);
    });
}

// see http://underscorejs.org/#throttle
function throttle(func, wait, options) {
    var timeout, context, args, result;
    var previous = 0;
    if (!options) {
        options = {};
    }

    var later = function() {
        previous = options.leading === false ? 0 : (new Date).getTime();
        timeout = null;
        result = func.apply(context, args);
        if (!timeout) {
            context = args = null;
        }
    };

    var throttled = function() {
        var now = (new Date).getTime();
        if (!previous && options.leading === false) {
            previous = now;
        }
        var remaining = wait - (now - previous);
        context = this;
        args = arguments;
        if (remaining <= 0 || remaining > wait) {
            if (timeout) {
                clearTimeout(timeout);
                timeout = null;
            }
            previous = now;
            result = func.apply(context, args);
            if (!timeout) {
                context = args = null;
            }
        } else if (!timeout && options.trailing !== false) {
            timeout = setTimeout(later, remaining);
        }
        return result;
    };

    throttled.cancel = function() {
        clearTimeout(timeout);
        previous = 0;
        timeout = context = args = null;
    };

    return throttled;
}

var updateCanvasSize = {
    directives: {
        'update-canvas-size': {
            inserted: function (el, binding) {
                var isPen = binding.value === 'pen';
                var ctx = el.getContext('2d');
                var $canvas = $(el);
                $canvas.data('steps', []);

                var width = $canvas.outerWidth();
                var height = $canvas.outerHeight();
                $canvas.attr({
                    width: width,
                    height: height
                });

                var timer;
                $(window).resize(function () {
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        var $wrap = $canvas.parent();
                        $canvas.attr({
                            width: $wrap.outerWidth(),
                            height: $wrap.outerHeight()
                        });
                        
                        if(isPen) {
                            var scaleX = $wrap.outerWidth() / width;
                            var scaleY = $wrap.outerHeight() / height;
                            el.dataset.scaleX = scaleX;
                            el.dataset.scaleY = scaleY;
                            ctx.save();
                            ctx.scale(scaleX, scaleY);
                            redraw(el);
                        }
                    }, 300);
                });
            }
        }
    }
};

if(getEnv() === 'production') {
    Vue.config.silent = true;
    Vue.config.productionTip = false;
    Vue.config.errorHandler = $.noop;
}

app.common = {
    urlMap: urlMap[getEnv()],
    mountDialog: mountDialog,
    asyncComponent: asyncComponent,
    throttle: throttle,
    updateCanvasSize: updateCanvasSize
};

})(app);

