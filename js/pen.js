'use strict';

(function (app) {
var pen = {
    data: function () {
        return {
            btnTimer: null,
            btnStatus: 'show',
            picture: null,
            enablePen: false,
            drawExpand: false,
            widthList: [2, 4, 6],
            widthIndex: 0,
            colorList: ['ff0000', 'ffed00', '0018ff', '1cff00'],
            colorIndex: 0,
            videoTimer: null,
            dragTimer: null,
            targetTouches: [],
            startX: 0,
            startY: 0,
            moving: false
        };
    },
    computed: {
        selected: function () {
            return this.drawExpand ? 'pen': 'bg';
        },
        width: function () {
            return this.widthList[this.widthIndex];
        },
        color: function () {
            return this.colorList[this.colorIndex];
        }
    },
    watch: {
        video: function (newValue) {
            var context = this;
            context.$nextTick(context.reset);
            if(newValue && newValue.action === 'snapshoot') {
                context.snapshoot();
            }
        }
    },
    methods: {
        reset: function () {
            var context = this;
            context.picture = null;
            context.drawExpand = true;
            context.widthIndex = 0;
            context.colorIndex = 0;
            context.clear();
        },
        showBtn: function () {
            clearTimeout(this.btnTimer);
            this.btnStatus = 'show';
        },
        hideBtn: function () {
            var context = this;
            context.btnTimer = setTimeout(function () {
                context.btnStatus = 'hide';
            }, 3000);
        },
        snapshoot: function () {
            this.picture = {};
        },
        changeWidth: function () {
            this.widthIndex = (++this.widthIndex) % this.widthList.length;
        },
        changeColor: function () {
            this.colorIndex = (++this.colorIndex) % this.colorList.length;
        },
        updateCanvasStyle: function (canvas) {
            var ctx = canvas.getContext('2d');
            ctx.strokeStyle = '#' + this.color;
            ctx.lineJoin = 'round';
            ctx.lineWidth = this.width;
        },
        dragstart: function (event) {
            dragstart(this, event);
        },
        dragmove: function (event) {
            dragmove(this, event);
        },
        dragend: function () {
            this.moving = false;
            this.targetTouches = [];
        },
        clear: function () {
            var canvas = this.$refs['pen-canvas'];
            if(!canvas) {
                return;
            }
            var steps = $(canvas).data('steps');
            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, 999999, 999999);
            steps.length = 0;
        },
        share: function () {
            var context = this;
            var videoCanvas = context.$refs['video-canvas'];
            var penCanvas = context.$refs['pen-canvas'];
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            canvas.width = penCanvas.width;
            canvas.height = penCanvas.height;

            var videoImg = new Image();
            videoImg.onload = function () {
                ctx.drawImage(this, 0, 0);
                var penImg = new Image();
                penImg.onload = function () {
                    ctx.drawImage(this, 0, 0);
                    var src = canvas.toDataURL('image/jpeg', 0.8);
                    var text = '中方愿同巴方一道努力，在政治上继续坚定相互支持，加强协调配合，保持高层交往';
                    app.share(src, text);
                };
                penImg.src = penCanvas.toDataURL();
            };
            videoImg.src = videoCanvas.toDataURL();
        },
        updateSize: function () {
            $(window).trigger('resize');
        }
    }
};

function dragstart(context, event) {
    context.moving = true;
    if(event.type === 'touchstart') {
        context.targetTouches = event.targetTouches;
        return;
    }
    context.updateCanvasStyle(event.target);
    var canvas = event.target;
    var ctx = canvas.getContext('2d');
    var steps = $(canvas).data('steps');

    steps.push({
        action: 'attr',
        list: {
            strokeStyle: '#' + context.color,
            lineJoin: 'round',
            lineWidth: context.width
        }
    });
    ctx.beginPath();
    steps.push({
        action: 'beginPath'
    });
    var position = canvas.getBoundingClientRect();
    var x = event.pageX - position.left;
    var y = event.pageY - position.top;
    context.startX = x;
    context.startY = y;
    if(context.drawExpand || context.enablePen) {
        var dataset = canvas.dataset;
        var scaleX = parseFloat(dataset.scaleX || 1);
        var scaleY = parseFloat(dataset.scaleY || 1);
        x /= scaleX;
        y /= scaleY;
        ctx.moveTo(x, y);
        ctx.stroke();
        steps.push(
            {
                action: 'moveTo',
                x: x,
                y: y
            },
            {
                action: 'stroke'
            }
        );
    }
}

function dragmove(context, event) {
    var isMultiTouch = context.targetTouches.length > 1;
    if(!context.moving || isMultiTouch) {
        return;
    }
    var canvas = event.target;
    var steps = $(canvas).data('steps');
    var ctx = canvas.getContext('2d');
    var position = canvas.getBoundingClientRect();
    var x = event.pageX - position.left;
    var y = event.pageY - position.top;
    if(context.drawExpand || context.enablePen) {
        var dataset = canvas.dataset;
        var scaleX = parseFloat(dataset.scaleX || 1);
        var scaleY = parseFloat(dataset.scaleY || 1);
        x /= scaleX;
        y /= scaleY;
        ctx.lineTo(x, y);
        ctx.stroke();
        steps.push(
            {
                action: 'lineTo',
                x: x,
                y: y
            },
            {
                action: 'stroke'
            }
        );
    } else {
        var left = x - context.startX;
        var top = y - context.startY;
        context.$emit('move', left, top);
        clearInterval(context.dragTimer);
        context.dragTimer = setInterval(function () {
            if(context.moving) {
                context.$emit('move', left, top);
            } else {
                clearInterval(context.dragTimer);
            }
        }, 300);
    }
}

app.pen = pen;
})(app);

