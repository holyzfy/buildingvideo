'use strcit';

(function (app) {

var components = app.components;

function start() {
    app.instance = new Vue({
        el: '#app',
        components: {
            maps: components.getMap,
            search: components.getSearch,
            views: components.getView
        },
        data: {
            view: null
        }
    });
}

app.start = start;

})(app);
