'use strcit';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getMap(resolve, reject) {
    var options = {
        name: 'map',
        template: 'templates/map.html',
        data: function () {
            return {
                map: null,
                multilevel: {},
                selectedId: [],
                showButtonTop: false
            };
        },
        computed: {
            selected: function () {
                var context = this;
                var list = context.selectedId;
                if(list.length > 0) {
                    var others = list.map(function (id, index) {
                        return context.getLevelByPath(list.slice(0, index + 1));
                    });
                    return [context.multilevel].concat(others); 
                } else {
                    return [context.multilevel];
                }
            },
            lastSelected: function () {
                return this.selected.slice(-1)[0];
            }
        },
        mounted: function () {
            var context = this;
            context.initMap();
            context.getMultilevel();
        },
        watch: {
            selected: function (newValue, oldValue) {
                var context = this;

                var lastItemId = newValue[newValue.length - 1].id;
                var enableMove = oldValue.filter(function (item) {
                    return item.id === lastItemId;
                }).length === 0;

                var map = context.map;
                map.clearOverlays();
                newValue.forEach(function (one, index) {
                    one.level && one.level.forEach(function (item, i) {
                        if(!item.point) {
                            return;
                        }
                        var point = new BMap.Point(item.point.lng, item.point.lat);
                        enableMove && (item.id === context.lastSelected.id) && map.panTo(point);
                        var icon = createPinIcon('images/pin1.svg');
                        var marker = new BMap.Marker(point, {icon: icon});
                        marker.addEventListener('click', function () {
                            context.setId(index, item.id);
                        });
                        map.addOverlay(marker);
                    });
                });
            }
        },
        directives: {
            scrollintoview: function (el, binding) {
                var selected = binding.value;
                selected && el.scrollIntoView();
            }
        },
        methods: {
            initMap: function () {
                var context = this;
                var map = new BMap.Map('map-bd');
                map.centerAndZoom(new BMap.Point(116.404, 39.915), 16);
                map.addControl(new BMap.MapTypeControl());
                map.setCurrentCity('北京');
                map.enableScrollWheelZoom(true);
                context.map = map;
            },
            getMultilevel: function () {
                var context = this;
                return $.getJSON(common.urlMap.multilevel, function (result) {
                    if(result.status !== 0) {
                        return;
                    }
                    context.multilevel = result.data;
                }); 
            },
            setId: function (index, id) {
                var context = this;
                var firstId = context.selectedId[index];
                var firstRemoved = context.selected.filter(function (item) {
                    return firstId && (item.id === firstId);
                })[0];
                var hasLevel = firstRemoved && firstRemoved.level && firstRemoved.level.length > 0;
                var removedIdList = context.selectedId.splice(index);
                if(hasLevel && removedIdList.length > 0) {
                    setTimeout(function () {
                        context.selectedId.push(id);
                    }, 500);
                } else {
                    context.selectedId.push(id);
                }
            },
            getLevelByPath: function (idPath) {
                var result = this.multilevel;
                idPath.forEach(function (id) {
                    result = result.level.filter(function (item) {
                        return item.id === id;
                    })[0];
                });
                return result;
            },
            reset: function () {
                this.selectedId = [];
            },
            scroll: (function () {
                var scrollTimer;
                return function () {
                    var context = this;
                    var wrap = context.$refs['multilevel-result'];
                    clearTimeout(scrollTimer);
                    scrollTimer = setTimeout(function () {
                        context.showButtonTop = $(wrap).scrollTop() > 0;
                    }, 100);
                };
            })(),
            scrollTop: function () {
                var wrap = this.$refs['multilevel-result'];
                $(wrap).animate({
                    scrollTop: 0
                });
            },
            setView: function (item) {
                app.instance.view = item; 
            }
        }
    };
    asyncComponent(options, resolve, reject);
}

function createPinIcon(url) {
    var icon = new BMap.Icon(url, new BMap.Size(17, 28));
    icon.setImageSize(new BMap.Size(17, 28));
    return icon;
}

app.components.getMap = getMap;

})(app);

