'use strict';

(function (app) {

var common = app.common;

function popupMark() {
    var options = {
        name: 'mark',
        template: 'templates/popup/mark.html',
        data: function () {
            return {
                selected: true
            };
        },
        methods: {
            close: function () {
                this.selected = false;
            },
            submit: function () {
                this.close();
            }
        }
    };
    common.mountDialog(options, function (instance) {
        app._debug.popupMark = instance;
    });
}

app.popupMark = popupMark;

})(app);
