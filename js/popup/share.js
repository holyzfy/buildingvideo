'use strict';

(function (app) {

var common = app.common;

function popupShare() {
    var options = {
        name: 'share',
        template: 'templates/popup/share.html',
        data: function () {
            return {
                selected : true
            };
        },
        methods: {
            close: function () {
                this.selected = false;
            },
            submit: function () {
                var $select = $(this.$refs.list);
                alert($select.val().join(','));
                this.close();
            }
        },
        directives: {
            select2: {
                inserted: function (el) {
                    $(el).select2({
                        placeholder: '选择人员',
                        width: 'resolve'
                    });
                }
            }
        }
    };
    common.mountDialog(options, function (instance) {
        app._debug.popupShare = instance;
    });
}

app.popupShare = popupShare;

})(app);
